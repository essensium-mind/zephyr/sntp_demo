Overview
********

The is an Zephyr SNTP demo. The demo performs the following acts:

* Get current date/time from NTP server.
* Update internal RTC clock.
* Get current date/time from RTC and display in the console.


Requirements
************

Your board must:

#. Have an network connection via Ethernet or WiFi.
#. Have a RTC clock

You should also define `rtc` alias in the board device tree.

Building and Running
********************

First clone the repository in zephy parent directory. Then build and flash as follows:

.. code-block:: bash

    west build -p always -b nucleo_h723zg workspace/sntp_demo/

